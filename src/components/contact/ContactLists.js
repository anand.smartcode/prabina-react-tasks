import React, { Component } from 'react';
import { connect } from 'react-redux';

import ContactFeed from './ContactFeed';

class ContactLists extends Component {
    render(){
        const { posts } = this.props.posts;
        let postContent;

        if (posts === null) {
          postContent = 'Data Empty. Please post contact form for data';
        } else {
          postContent = <ContactFeed posts={posts} />;
        }
    
        return (
          <div className="feed">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  {postContent}
                </div>
              </div>
            </div>
          </div>
        )
    }
}

const mapStateToProps = state => ({
	posts: state.posts
})

export default connect(mapStateToProps)(ContactLists);